# Используем базовый образ Python
FROM python:3.8

# Устанавливаем рабочую директорию в контейнере
WORKDIR /

# Копируем ваш Python-файл бота в контейнер
COPY / /

# Устанавливаем зависимости, если они есть (requirements.txt)
RUN pip install --no-cache-dir -r requirements.txt

# Команда для запуска вашего бота
CMD ["python", "chatgpt.py"]
